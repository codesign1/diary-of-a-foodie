<footer>
    <div class="grid-xs-12">
        <div class="footer-container">
            <img src="<?php echo get_template_directory_uri() ?>/img/gabriela_logo.svg">
            <h2>GABRIELA CALDERÓN</h2>
            <p class="contact-info">
                gabrielacalderonbu@hotmail.com - 316.322.7702
            </p>
            <p class="codesign-info">
                CREADO CON AMOR POR <a href="http://estudiocodesign.com">ESTUDIO CODESIGN</a> - TODOS LOS DERECHOS
                RESERVADOS 2016
                <br>
                +(57) 315 848 8331 - CARRERA 8 #67-74 - BOGOTÁ, COLOMBIA
            </p>
        </div>
    </div>
</footer>
<?php wp_footer(); ?>
<?php $map = get_field('geographical_location', get_the_ID());?>
<script type="text/javascript"> 

  function initialize() {
        var myLatlng = new google.maps.LatLng(<?php echo $map['lat']; ?>, <?php echo $map['lng']; ?>);
        var myOptions = {
          zoom: 15,
          center: myLatlng,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        }

        var map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
        var marker = new google.maps.Marker({
          map: map,
          position: myLatlng,
        });
      }

      function loadScript() {
        var script = document.createElement("script");
        script.type = "text/javascript";
        script.src = "http://maps.google.com/maps/api/js?key=AIzaSyBiEMQ_fgXFrTDcP-RPrN6xL3e_COvsJEY&sensor=false&callback=initialize";
        document.body.appendChild(script);
      }

      window.onload = loadScript;
    </script>
</body>
</html>