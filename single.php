<?php get_header();
if (have_posts()) : while (have_posts()) : the_post();
    $post_type = $wp_query->query['post_type']; ?>

    <!--corresponding top menu-->
    <div class="row review-top-row">
        <div class="col">
            <a class="yellow-button" href="<?php echo get_post_type_archive_link($post_type); ?>">Back to all
                <?php echo($post_type === 'doaf_recipe' ? 'recipes' : 'reviews') ?></a>
        </div>
    </div>
    <div class="row review-title-row">
        <h1 class="yellow-bar-title"><?php the_title(); ?></h1>
    </div>
    <div class="row review-info-row">
        <div class="col-md-9 col-xs-12">
            <div class="content-hero-img">
                <img style="width: 100%;" src="<?php the_post_thumbnail_url(); ?>">
                <a target="_blank"
                   href="http://www.facebook.com/sharer/sharer.php?u=<?php echo "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>"
                   class="facebook-hero-img"><img
                        src="<?php echo get_template_directory_uri() . "/img/facebook_share_logo.svg" ?>"></a>
            </div>
        </div>
        <div class="col-md-3 col-xs-12 review-info">
            <?php if ($post_type === 'doaf_review') { ?>
                <p class="review-rating"><?php echo get_the_terms(get_the_ID(), 'doaf_rating')[0]->name; ?></p>
                <p class="review-date"><?php the_time('d.m.y'); ?></p>
                <p class="review-location"><?php echo get_the_terms(get_the_ID(), 'doaf_review_location')[0]->name; ?></p>
                <p class="review-price-range"><?php echo get_the_terms(get_the_ID(), 'doaf_price_range')[0]->name; ?></p>
            <?php } else if ($post_type === 'doaf_recipe') { ?>
                <p class="review-date"><?php the_field('serves'); ?></p>
                <p class="review-location"><?php the_field('preparation_time'); ?></p>
                <p class="review-date"><?php the_field('difficulty_level'); ?></p>
            <?php } else if ($post_type === 'doaf_wine_review') { ?>
                <p class="review-date"><?php the_time('d.m.y'); ?></p>
                <p><?php the_field('wine_origin'); ?></p>
                <p class="wine-price"><?php the_field('wine_price'); ?></p>
            <?php } ?>
        </div>
    </div>
    <div class="row review-row">
        <div class="col-md-9 col-xs-12">
            <?php the_content(); ?>
            <?php $mapData = get_field('geographical_location', get_the_ID()); 
            if(isset($mapData['lat']) && !empty($mapData['lat'])){?>
            <!-- Mapa -->
            <div id="map_canvas" style="height: 350px;width: 100%;"></div>
            <?php }?>
        </div>
    </div>

    <!--row containing the next review/recipe button, which is conditional-->
    <div class="row next-content-row">
        <?php previous_post_link('%link', 'View Next ' . ($post_type === 'doaf_recipe' ? 'Recipe' : 'Review')); ?>
    </div>

<?php endwhile;

else:
    _e('Sorry, no pages matched your criteria.', 'textdomain');
endif;

get_footer(); ?>
