<?php get_header(); ?>

<!--main content-->
<div class="row">
    <div class="grid-filters-container">
        <form id="filters-form" method="post" class="grid-filters-selectors">
            <!--use the taxonomies' names to loop-->
            <?php $taxonomies = get_object_taxonomies('doaf_recipe', 'objects');
            foreach ($taxonomies as $taxonomy => $object) {
                $label = $object->labels->name;
                if ($taxonomy !== 'doaf_featured') { ?>
                    <select class="select-theme-dark" onchange="filterValueChanged(this);"
                            name="<?php echo $taxonomy . '_value'; ?>">

                        <?php $terms = get_terms(array(
                            'taxonomy' => $taxonomy,
                            'hide_empty' => false
                        )); ?>

                        <option value disabled selected><?php echo $label; ?></option>
                        <option value="all"><?php _e('[:en]All[:es]Todo'); ?></option>
                        <?php foreach ($terms as $term) { ?>
                            <option <?php echo(doaf_get_request_var($taxonomy . '_value') === $term->slug
                                ? 'selected' : ''); ?>
                                value="<?php echo $term->slug; ?>"><?php echo $term->name; ?></option>
                        <?php } ?>
                    </select>
                <?php }
            } ?>
        </form>
    </div>
</div>

<!--taxonomy queries-->
<?php
$tax_query = null;
foreach ($_POST as $key => $val) {
    if (isset($key) && $val !== 'all') {
        $tax_query = array(
            array(
                'taxonomy' => str_replace('_value', '', $key),
                'field' => 'slug',
                'terms' => array($val)
            )
        );
    }
} ?>

<div class="row" id="masonry-container">
    <div class="grid-container">
        <?php
        // check which post type this is an archive of & make the query
        $args = array(
            'post_type' => $wp_query->query['post_type'],
            'tax_query' => $tax_query
        );
        $loop = new WP_Query($args);
        if ($loop->have_posts()): while ($loop->have_posts()) : $loop->the_post(); ?>
            <div class="grid-item">
                <div class="img-container">
                    <img src="<?php the_post_thumbnail_url('large'); ?>">
                </div>
                <div class="info-container">
                    <span class="date"><?php the_time('d.m.y'); ?></span>
                    <div>
                        <h3><?php the_title(); ?></h3>
                    </div>
                </div>
                <a href="<?php the_permalink(); ?>"></a>
            </div>
        <?php endwhile;
        else: ?>
            <div class="grid-item">
                <h3><?php _e("[:en]Sorry, we couldn't find anything using those filters.
                [:es]Lo sentimos, no pudimos encontrar nada utilizando los filtros seleccionados."); ?></h3>
            </div>
        <?php endif; ?>
    </div>
</div>

<?php get_footer(); ?>
