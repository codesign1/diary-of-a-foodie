<?php get_header(); ?>

<div class="row explore-menu-row">
    <div class="col-xs-12 col-md-4">
        <div class="explore-menu-element">
            <a href="/explore-categories/around-the-world"></a>
            <img src="<?php echo get_template_directory_uri() ?>/img/compass_logo.svg">
            <p><?php echo get_term_by('slug','around-the-world', 'doaf_explore_category')->name; ?></p>
        </div>
    </div>
    <div class="col-xs-12 col-md-4">
        <div class="explore-menu-element">
            <a href="/explore-categories/perfect-moments"></a>
            <img src="<?php echo get_template_directory_uri() ?>/img/gabriela_logo.svg">
            <p><?php echo get_term_by('slug', 'perfect-moments', 'doaf_explore_category')->name; ?></p>
        </div>
    </div>
    <div class="col-xs-12 col-md-4">
        <div class="explore-menu-element">
            <a href="/explore-categories/important-opinions"></a>
            <img src="<?php echo get_template_directory_uri() ?>/img/chefs_hat.svg">
            <p><?php echo get_term_by('slug', 'important-opinions', 'doaf_explore_category')->name; ?></p>
        </div>
    </div>
</div>

<div class="row explore-quote-row">
    <img src="<?php the_field('quote_background_image') ?>">
    <div class="quote-text-container">
        <img id="left-quote" src="<?php echo get_template_directory_uri() ?>/img/left-quote.svg">
        <p class="quote-p">
            <?php the_field('quote_text') ?>
        </p>
        <img id="right-quote" src="<?php echo get_template_directory_uri() ?>/img/right-quote.svg">
    </div>
</div>

<?php get_footer(); ?>
