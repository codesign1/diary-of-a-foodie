<?php get_header(); ?>

<div class="row text-center about-me-title">
    <h1 class="yellow-bar-title">Gabriela Calderón</h1>
</div>
<div class="about-me">
    <div class="row">
        <div class="col-md-4 col-xs-12 img-container">
            <img src="<?php the_field('about_me_image') ?>">
        </div>
        <div class="col-md-8 col-xs-12">
            <?php while (have_posts()) : the_post(); ?>
                <?php the_content(); ?>
            <?php endwhile;
            wp_reset_query();
            ?>
        </div>
    </div>
</div>

<?php get_footer(); ?>
