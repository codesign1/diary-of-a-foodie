/**
 * Created by jsrolon on 02-11-2016.
 */

function es() {
    if(location.pathname.match(/^(\/en|\/es)/)) {
        location.pathname = location.pathname.replace(/^\/en/, '/es');
    } else {
        location.pathname = '/es' + location.pathname;
    }
}

function en() {
    if(location.pathname.match(/^(\/en|\/es)/)) {
        location.pathname = location.pathname.replace(/^\/es/, '/en');
    } else {
        location.pathname = '/en' + location.pathname;
    }
}
