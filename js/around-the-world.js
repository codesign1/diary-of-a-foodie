/**
 * Created by jsrolon on 04-11-2016.
 */

var TRANSITION_TIME = 300;

jQuery('document').ready(function() {
    var items = jQuery('.around-the-world-item');
    items.map(function(i, o) {
        var item = jQuery(o);
        var title = item.children('h4');
        var list = item.children('ol');

        title.click(function() {
            var newOpacity = (parseInt(list.css('opacity')) + 1) % 2;

            if(newOpacity) { // showing
                list.css('display', 'block');
                setTimeout(function() {
                    list.css('opacity', newOpacity);
                }, TRANSITION_TIME);
            } else { // hiding
                setTimeout(function() {
                    list.css('display', 'none');
                }, TRANSITION_TIME);
                list.css('opacity', newOpacity);
            }
        });
    });
});
