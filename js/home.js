/**
 * Created by jsrolon on 28-10-2016.
 */

jQuery('document').ready(function() {
    jQuery('.home-main').slick({
        arrows: false,
        autoplay: true,
	pauseOnHover: false
    });
});
