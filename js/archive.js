/**
 * Created by jsrolon on 01-11-2016.
 */

jQuery(document).ready(function() {
    jQuery('select').select2({
        minimumResultsForSearch: -1
    });
});

function filterValueChanged(element) {
    jQuery("#filters-form > select").each(function() {
        if(this !== element) {
            jQuery(this).val('');
        }
    });
    element.form.submit();
}
