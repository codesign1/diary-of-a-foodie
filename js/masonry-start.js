/**
 * Created by jsrolon on 31-10-2016.
 */

jQuery('document').ready(function() {
    var masonryGrid = jQuery('#masonry-container').masonry({
        itemSelector: '.grid-item',
        columnWidth: 420
    });
    masonryGrid.imagesLoaded().progress( function() {
        masonryGrid.masonry('layout');
    });
});
