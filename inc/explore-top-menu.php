<div class="row">
    <div class="col-md-12">
        <div class="grid-filters-container">
            <a class="filters-all-button"
               href="/explore-categories/around-the-world"><?php _e('[:en]The world[:es]El mundo'); ?></a>
            <a id="perfect-moments-button" class="filters-all-button"
               href="/explore-categories/perfect-moments"><?php _e('[:en]Perfect moments[:es]Momentos perfectos'); ?></a>
            <a id="important-opinions-button" class="filters-all-button"
               href="/explore-categories/important-opinions"><?php _e('[:en]Important opinions[:es]Opiniones importantes'); ?></a>
        </div>
    </div>
</div>
