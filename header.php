<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Diary of a Foodie</title>

    <?php wp_head(); ?>
</head>
<body>
<nav id="nav" <?php echo(is_home() ? 'class="home"' : ''); ?>>
    <div class="row top-nav">
        <div class="col-xs-6 col-md-3 last-md top-nav-left-btns">
            <ul class="top-left-nav-list">
                <li><a href="/">HOME</a></li>
                <li class="instagram-logo"><a href="https://www.instagram.com/thediaryofafoodie/"><img
                            src="<?php echo get_template_directory_uri(); ?>/img/instagram.svg" alt=""></a></li>
            </ul>
        </div>
        <div class="col-xs-12 first-xs last-md col-md-6 foodie-logo">
            <a href="/">
                <img src="<?php echo get_template_directory_uri(); ?>/img/logo.svg">
            </a>
        </div>
        <div class="col-xs-6 col-md-3 last-md top-nav-right-btns">
            <ul class="top-right-btn-list">
                <li><a id="english-button" onclick="en();">ENG</a></li>
                <li><a id="spanish-button" onclick="es();">ESP</a></li>
            </ul>
        </div>
    </div>
    <div class="row links-nav">
        <div class="col-xs-12">
            <?php wp_nav_menu(array(
                'menu_class' => 'navbar-links',
                'theme_location' => 'nav-menu'
            )); ?>
        </div>
    </div>
</nav>
