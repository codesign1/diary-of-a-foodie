<?php
/**
 * Created by PhpStorm.
 * User: jsrolon
 * Date: 28-10-2016
 * Time: 11:02 AM
 */

function doaf_theme_setup() {
    add_theme_support('post-thumbnails');
}

function doaf_enqueue_styles() {
    wp_enqueue_style('futura', get_template_directory_uri() . '/fonts/futura/stylesheet.css');
    wp_enqueue_style('select-theme', get_template_directory_uri() . '/select2.min.css');
    wp_enqueue_style('grid', get_template_directory_uri() . '/flexboxgrid.min.css');
    wp_enqueue_style('core', get_template_directory_uri() . '/style.css');
    wp_enqueue_style('slick', get_template_directory_uri() . '/slick.css');
}

function doaf_enqueue_scripts() {
    // libraries
    wp_enqueue_script('jquery');

    // custom scripts
    wp_enqueue_script('langs', get_template_directory_uri() . '/js/localization.js', null, null, true);

    // conditional scripts
    if (is_home()) {
        wp_enqueue_script('slick', get_template_directory_uri() . '/js/plugins/slick.min.js', null, null, true);
        wp_enqueue_script('home', get_template_directory_uri() . '/js/home.js', null, null);
    }

    if (is_archive() || is_category()) {
        // select inputs
        wp_enqueue_script('select-js', get_template_directory_uri() . '/js/plugins/select2.full.min.js', null, null, true);
        wp_enqueue_script('select-boxes', get_template_directory_uri() . '/js/archive.js', null, null, true);

        // masonry layout
        wp_enqueue_script('masonry-minified', get_template_directory_uri() . '/js/plugins/masonry.min.js', null, null, true);
        wp_enqueue_script('masonry-imagesloaded', get_template_directory_uri() . '/js/plugins/imagesloaded.min.js', null, null, true);
        wp_enqueue_script('masonry-start', get_template_directory_uri() . '/js/masonry-start.js', null, null, true);
    }

    if (is_archive('doaf_w_restaurant')) {
        wp_enqueue_script('around-the-world', get_template_directory_uri() . '/js/around-the-world.js', null, null, true);
    }
}

function doaf_register_menus() {
    register_nav_menu('nav-menu', 'Navigation Menu');
}

function doaf_register_custom_post_types() {
    // reviews
    register_post_type('doaf_review', array(
        'labels' => array(
            'name' => __('[:en]Reviews[:es]Reseñas'),
            'singular_name' => __('[:en]Review[:es]Reseña'),
        ),
        'description' => 'Food and/or restaurant reviews.',
        'public' => true,
        'supports' => array('title', 'editor', 'thumbnail'),
        'taxonomies' => array(
            'doaf_cuisine', 'doaf_featured'
        ),
        'menu_position' => 5,
        'rewrite' => array(
            'slug' => 'review'
        ),
        'has_archive' => true
    ));

    // recipes
    register_post_type('doaf_recipe', array(
        'labels' => array(
            'name' => __('[:en]Recipes[:es]Recetas'),
            'singular_name' => __('[:en]Recipe[:es]Receta'),
        ),
        'description' => 'Food recipes.',
        'public' => true,
        'supports' => array('title', 'editor', 'thumbnail'),
        'taxonomies' => array(
            'doaf_cuisine', 'doaf_featured'
        ),
        'menu_position' => 5,
        'rewrite' => array(
            'slug' => 'recipe'
        ),
        'has_archive' => true
    ));

    // explore content
    register_post_type('doaf_explore', array(
        'labels' => array(
            'name' => __('[:en]Explore[:es]Explora'),
            'singular_name' => __('[:en]Explore[:es]Explora'),
        ),
        'description' => 'Explore.',
        'public' => true,
        'supports' => array('title', 'editor', 'thumbnail'),
        'taxonomies' => array(
            'doaf_explore_category', 'doaf_featured'
        ),
        'menu_position' => 5,
        'rewrite' => array(
            'slug' => 'explore',
        )
    ));

    // wine reviews
    register_post_type('doaf_wine_review', array(
        'labels' => array(
            'name' => __('[:en]Wines[:es]Vinos'),
            'singular_name' => __('[:en]Wine[:es]Vino'),
        ),
        'description' => 'Wine reviews.',
        'public' => true,
        'supports' => array('title', 'editor', 'thumbnail'),
        'taxonomies' => array(
            'doaf_featured'
        ),
        'menu_position' => 5,
        'rewrite' => array(
            'slug' => 'wine'
        ),
        'has_archive' => true
    ));

    // world restaurant
    register_post_type('doaf_w_restaurant', array(
        'labels' => array(
            'name' => __('[:en]Restaurants[:es]Restaurantes'),
            'singular_name' => __('[:en]Restaurant[:es]Restaurante'),
        ),
        'description' => 'World restaurants.',
        'public' => true,
        'supports' => array('title', 'editor', 'thumbnail'),
        'taxonomies' => array(
            'doaf_world_city'
        ),
        'menu_position' => 5,
        'rewrite' => array(
            'slug' => 'explore-categories/around-the-world'
        ),
        'has_archive' => true
    ));

    flush_rewrite_rules();
}

function doaf_remove_default_post_type() {
    remove_menu_page('edit.php');
}

function doaf_register_custom_taxonomies() {
    register_taxonomy('doaf_cuisine', array('doaf_review', 'doaf_recipe'), array(
        'label' => __('[:en]Cuisine[:es]Tipo de Cocina')
    ));

    register_taxonomy('doaf_review_location', array('doaf_review'), array(
        'label' => __('[:en]Location[:es]Lugar'),
        'hierarchical' => true
    ));

    register_taxonomy('doaf_price_range', array('doaf_review'), array(
        'label' => __('[:en]Price[:es]Precio'),
    ));

    register_taxonomy('doaf_rating', array('doaf_review'), array(
        'label' => __('[:en]Rating[:es]Calificación'),
    ));

    register_taxonomy('doaf_grapes', array('doaf_wine_review'), array(
        'label' => __('[:en]Grapes[:es]Uvas'),
        'hierarchical' => true
    ));

    register_taxonomy('doaf_explore_category', array('doaf_explore'),
        array(
            'label' => __('[:en]Category[:es]Categoría'),
            'rewrite' => array(
                'slug' => 'explore-categories'
            )
        ));

    register_taxonomy('doaf_featured', array('doaf_review', 'doaf_recipe', 'doaf_explore', 'doaf_wine_review'), array(
        'label' => __('[:en]Featured[:es]Destacado')
    ));

    register_taxonomy('doaf_world_city', array('doaf_w_restaurant'), array(
        'label' => __('[:en]Cities[:es]Ciudades'),
        'hierarchical' => true
    ));
}

function doaf_get_request_var($var) {
    return isset($_POST[$var]) ? $_POST[$var] : null;
}

function add_new_image_sizes() {
    add_image_size('doaf_full_hd', 1920, 1080);
}

/**
 * Stuff to be done on init
 */
add_action('init', 'doaf_register_menus');
add_action('init', 'doaf_register_custom_post_types');

// register all the custom taxonomies
add_action('init', 'doaf_register_custom_taxonomies');

/**
 * Script and style enqueueing
 */
add_action('wp_enqueue_scripts', 'doaf_enqueue_styles');
add_action('wp_enqueue_scripts', 'doaf_enqueue_scripts');

/**
 * After theme setup
 */
add_action('after_setup_theme', 'doaf_theme_setup');

/**
 * Admin menu modifications
 */
add_action('admin_menu', 'doaf_remove_default_post_type');

/**
 * After theme setup
 */
add_action('after_setup_theme', 'add_new_image_sizes');
