<?php get_header(); ?>

<!--menu-->
<?php include_once 'inc/explore-top-menu.php' ?>

<div class="row" id="masonry-container">
    <div class="grid-container">
        <?php
        // check which post type this is an archive of & make the query
        $args = array(
            'post_type' => 'doaf_explore',
            'tax_query' => array(
                array(
                    'taxonomy' => 'doaf_explore_category',
                    'field' => 'slug',
                    'terms' => $wp_query->query['doaf_explore_category']
                )
            )
        );
        $loop = new WP_Query($args);
        if ($loop->have_posts()): while ($loop->have_posts()) : $loop->the_post(); ?>
            <div class="grid-item">
                <div class="img-container">
                    <img src="<?php the_post_thumbnail_url('large'); ?>">
                </div>
                <div class="info-container">
                    <span class="date"><?php the_time('d.m.y'); ?></span>
                    <div>
                        <h3><?php the_title(); ?></h3>
                    </div>
                </div>
                <a href="<?php the_permalink(); ?>"></a>
            </div>
        <?php endwhile;
        else: ?>
            <div class="grid-item">
                <h3><?php _e("[:en]Sorry, we couldn't find anything using those filters.
                [:es]Lo sentimos, no pudimos encontrar nada utilizando los filtros selccionados."); ?></h3>
            </div>
        <?php endif; ?>
    </div>
</div>

<?php get_footer(); ?>
