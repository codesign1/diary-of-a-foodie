<?php get_header(); ?>

<!--top menu-->
<?php include_once 'inc/explore-top-menu.php' ?>

<div class="row">
    <div class="restaurant-list-container">
        <h3 class="around-the-world-title"><?php _e('[:en]Top 10 restaurants in cities around the world[:es]Top 10 de restaurantes en ciudades alrededor del mundo'); ?></h3>
        <ul>
            <?php $cities = get_categories(array(
                'taxonomy' => 'doaf_world_city',
                'hide_empty' => false));

            foreach ($cities as $city) { ?>
                <li class="around-the-world-item">
                    <h4><?php echo $city->name; ?></h4>
                    <?php $args = array(
                        'post_type' => 'doaf_w_restaurant',
                        'tax_query' => array(
                            array(
                                'taxonomy' => 'doaf_world_city',
                                'field' => 'slug',
                                'terms' => $city->slug
                            )
                        ),
                        'meta_key' => 'position',
                        'orderby' => 'meta_value_num',
                        'order' => 'ASC'
                    );
                    $loop = new WP_Query($args);
                    if ($loop->have_posts()) : ?>
                        <ol style="display: none;">
                            <?php while ($loop->have_posts()) : $loop->the_post(); ?>
                                <li>
                                    <div class="restaurant-info-wrapper">
                                        <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                        <span
                                            class="restaurant-address"><?php the_field('restaurant_address'); ?></span>
                                    </div>
                                </li>
                            <?php endwhile; ?>
                        </ol>
                    <?php else: ?>
                        <h3><?php _e("[:en]Sorry, we couldn't find anything.
                [:es]Lo sentimos, no pudimos encontrar nada."); ?></h3>
                    <?php endif; ?>
                </li>
            <?php } ?>
        </ul>
    </div>
</div>

<?php get_footer(); ?>
