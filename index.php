<?php get_header(); ?>

<!--main content-->
<div class="home-main">
    <?php // check which post type this is an archive of & make the query
    $args = array(
        'post_type' => array('doaf_review', 'doaf_recipe', 'doaf_explore', 'doaf_wine_review'),
        'tax_query' => array(
            array(
                'taxonomy' => 'doaf_featured',
                'field' => 'slug',
                'terms' => 'featured'
            )
        )
    );
    $loop = new WP_Query($args);
    if ($loop->have_posts()): while ($loop->have_posts()) : $loop->the_post(); ?>
    <div class="home-item">
        <img src="<?php the_post_thumbnail_url('doaf_full_hd'); ?>">
        <div class="home-item-text">
            <h2><?php the_title() ?></h2>
            <a href="<?php the_permalink(); ?>"><?php _e('[:en]Read[:es]Leer'); ?></a>
        </div>
    </div>
    <?php endwhile;
    else: ?>
        <div class="grid-item">
            <h3><?php _e("[:en]Sorry, we couldn't find anything using those filters.
                [:es]Lo sentimos, no pudimos encontrar nada utilizando los filtros selccionados."); ?></h3>
        </div>
    <?php endif; ?>
</div>

<?php get_footer(); ?>
